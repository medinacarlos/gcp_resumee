window.addEventListener('DOMContentLoaded',(event)=>{ 
    getVisitCount();
});

const FunctionApiURL = 'https://europe-west1-gcp-resume-376014.cloudfunctions.net/ViewerFunction';

const getVisitCount = () => {
    fetch(FunctionApiURL).then(response =>{ // get the data from the URL that call the Cloud Function
        return response.text()
    }).then(response =>{
        console.log("Website call Cloud Function API.");
        let count = response.split(':')[1].trim();
        document.getElementById("counter").innerText = count; 
    }).catch(function(error){
        console.log(error);
    });
}
