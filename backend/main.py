from google.cloud import firestore

def get_visit_count(request):

    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Max-Age': '3600'
    }
    db = firestore.Client()
    doc_ref = db.collection(u'visitor_count').document(u'counter')
    doc = doc_ref.get()
    if doc.exists:
        count = int(doc.get(u'count'))
        count += 1
        doc_ref.update({u'count': count})
        return f'Visitor count: {count}', 200, headers
    else:
        return f'No such document!', 200, headers
